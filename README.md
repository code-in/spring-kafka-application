

	
	KAFKA_HOME = /home/ahmet/development-env/tools/apache-kafka_2.12-2.3.1

	Sırasıyla kafka ortamları ayağa kaldırılmalı topicler oluşturulmalıdır

	$KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties
	$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties

	$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic productevents
	$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic eventscalculations

	Daha sonra run.sh ile uygulama ayağa kaldırılabilir
