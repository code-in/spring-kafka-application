package com.ulger.api.product;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ProductRepository {

	private final List<Product> mockData = new ArrayList<Product>();
	
	public ProductRepository() {
		fillMockData();
	}
	
	public List<Product> findAll() {
		return mockData;
	}
	
	private void fillMockData() {
		mockData.add(new Product("CMP", "Computer"));
		mockData.add(new Product("PHN", "Phone"));
		mockData.add(new Product("CAM", "Camera"));
	}
}