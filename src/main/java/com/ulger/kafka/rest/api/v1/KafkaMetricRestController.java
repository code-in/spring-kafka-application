package com.ulger.kafka.rest.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ulger.kafka.config.KafkaConfig;
import com.ulger.kafka.service.KafkaService;

@RestController
@RequestMapping("/rest/api/v1/kafka")
public class KafkaMetricRestController {

	@Autowired
	private KafkaService kafkaService;
	
	@Autowired
	private KafkaConfig kafkaConfig;
	
	@PostMapping
	public void doPost(@RequestParam("recordBody") String recordBody) {
		kafkaService.writeKafkaRecord(recordBody, kafkaConfig.getSourceTopic());
	}
}