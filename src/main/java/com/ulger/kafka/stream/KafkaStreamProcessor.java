package com.ulger.kafka.stream;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ulger.kafka.config.KafkaConfig;

@Component
public class KafkaStreamProcessor {
       
	@Autowired
	public KafkaStreamProcessor(KafkaConfig kafkaConfig) {
    	Properties consumerProperties = getAsProperties(kafkaConfig);
		
		Serde<String> stringSerde = Serdes.String();
        StreamsBuilder builder = new StreamsBuilder();

        builder
			.stream(kafkaConfig.getSourceTopic(), Consumed.with(stringSerde, stringSerde))
			.to(kafkaConfig.getCalculationTopic());
        
        KafkaStreams streams = new KafkaStreams(builder.build(), consumerProperties);
        CountDownLatch latch = new CountDownLatch(1);
        
        Runtime.getRuntime().addShutdownHook(new Thread("streams-wordcount-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        new Thread(() -> {
            try {
                streams.start();
                latch.await();
            } catch (final Throwable e) {
            }
        });
	}
	
	private static Properties getAsProperties(KafkaConfig kafkaConfig) {
		Properties properties = new Properties();
		
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "productevents");
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getBroker());
		properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
		properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        
        return properties;
	}
}