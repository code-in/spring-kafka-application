package com.ulger.kafka.config;

class DefaultKafkaConfig implements KafkaConfig {

	private String name;
	private String broker;
	private String sourceTopic;
	private String calculationTopic;
	
	public DefaultKafkaConfig(String name, String broker, String sourceTopic, String calculationTopic) {
		this.name = name;
		this.broker = broker;
		this.sourceTopic = sourceTopic;
		this.calculationTopic = calculationTopic;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String getBroker() {
		return broker;
	}

	@Override
	public String getSourceTopic() {
		return sourceTopic;
	}
	
	@Override
	public String getCalculationTopic() {
		return calculationTopic;
	}
}