package com.ulger.kafka.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class KafkaConfiguration {

	@Value("${kafka.config.productevents.name}")
	private String name;
	
	@Value("${kafka.config.productevents.broker}")
	private String broker;

	@Value("${kafka.config.productevents.topic.source}")
	private String sourceTopic;

	@Value("${kafka.config.productevents.topic.calculation}")
	private String calculationTopic;
	
	@Bean
	public KafkaConfig kafkaConfig() {
		return new DefaultKafkaConfig(name, broker, sourceTopic, calculationTopic);
	}
}