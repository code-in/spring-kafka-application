package com.ulger.kafka.config;

public interface KafkaConfig {

	String getName();
	
	String getBroker();
	
	String getSourceTopic();
	
	String getCalculationTopic();
}