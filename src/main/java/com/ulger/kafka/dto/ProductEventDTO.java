package com.ulger.kafka.dto;

import java.util.Date;

public class ProductEventDTO {

	private Date date;
	private String productId;
	private String eventName;
	private String userId;
	
	public Date getDate() {
		return date;
	}
	
	public String getProductId() {
		return productId;
	}
	
	public String getEventName() {
		return eventName;
	}
	
	public String getUserId() {
		return userId;
	}
}