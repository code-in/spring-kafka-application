package com.ulger.kafka.dto;

import java.util.Date;

public class ProductEventCalculationDTO {

	private int productviewCount;
	private int purchaseCount;
	private int addtoBasketCount;
	private int removefrombasketCount;
	private Date receivedAt;

	public int getProductviewCount() {
		return productviewCount;
	}

	public int getPurchaseCount() {
		return purchaseCount;
	}

	public int getAddtoBasketCount() {
		return addtoBasketCount;
	}

	public int getRemovefrombasketCount() {
		return removefrombasketCount;
	}

	public Date getReceivedAt() {
		return receivedAt;
	}
}