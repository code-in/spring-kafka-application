package com.ulger.kafka.producer;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import com.ulger.kafka.config.KafkaConfig;

public class ProducerFactory {

	public static KafkaProducer<String, String> buildProducer(KafkaConfig kafkaConfig) {
		return new KafkaProducer<>(getAsProperties(kafkaConfig));
	}
	
	private static Properties getAsProperties(KafkaConfig kafkaConfig) {
		Properties properties = new Properties();
		
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getBroker());
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        
        return properties;
	}
}