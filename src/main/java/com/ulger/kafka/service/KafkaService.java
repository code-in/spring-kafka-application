package com.ulger.kafka.service;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ulger.kafka.config.KafkaConfig;
import com.ulger.kafka.producer.ProducerFactory;

@Component
public class KafkaService {

	private KafkaProducer<String, String> kafkaProducer;

	@Autowired
	public KafkaService(KafkaConfig kafkaConfig) {
		this.kafkaProducer = ProducerFactory.buildProducer(kafkaConfig);
	}
	
	public void writeKafkaRecord(String recordBody, String topic) {
		ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, recordBody);
		kafkaProducer.send(record);
		kafkaProducer.flush();
	}
}