package com.ulger.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumerListener {

    @Autowired
    private SimpMessagingTemplate template;

    @KafkaListener(topicPattern = "${kafka.config.productevents.topic.calculation}", groupId = "productevents")
    public void consume(@Payload String message) {
        this.template.convertAndSend("/topic/productevents", message);
    }
}