package com.ulger.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ulger.api.product.ProductService;

@Controller
@RequestMapping("/product")
public class ProductMvcController {

	@Autowired
	private ProductService productService;
	
	@GetMapping
	public ModelAndView doGet() {
		return new ModelAndView("products").addObject("allProducts", productService.getProducts());
	}
}