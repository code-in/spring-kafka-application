package com.ulger.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/kafka/dashboard")
public class KafkaMvcController {
	
	@GetMapping
	public ModelAndView doGet() {
		return new ModelAndView("kafkadashboard");
	}
}