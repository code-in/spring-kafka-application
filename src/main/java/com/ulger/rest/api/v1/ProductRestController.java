package com.ulger.rest.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ulger.api.product.Product;
import com.ulger.api.product.ProductService;

@RestController
@RequestMapping("/rest/api/v1/product")
public class ProductRestController {

	@Autowired
	private ProductService productService;
	
	@GetMapping
	public List<Product> listProducts() {
		return productService.getProducts();
	}
}