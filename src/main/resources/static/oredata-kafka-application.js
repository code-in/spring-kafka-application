(function(jQuery) {
	KafkaAPI = function() {
		var kafkaEndpoint = '/rest/api/v1/kafka';

		return {
			sendKafkaRecord : function(recordBody) {
				jQuery.ajax({
					url : kafkaEndpoint,
					type : 'POST',
					async : true,
					data : {
						recordBody : recordBody
					},
					success : function(data) {
					},
					error : function(e) {
					}
				});
			}
		}
	};

	jQuery.KafkaAPI = new KafkaAPI();
})(jQuery);